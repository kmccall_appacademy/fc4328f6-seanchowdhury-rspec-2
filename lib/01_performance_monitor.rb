def measure(n=1)
  start_time = Time.now.to_f
  n.times {yield}
  (Time.now.to_f - start_time) / n
end
